# serpfix

This is a Chrome extension that makes Google search result pages more
user-friendly.

It can hide text ads (which are often so numerous now that they push the first
organic result below the fold, sigh) and hide results from user-specified
domains.

The ["Web Search Engine" icon](https://www.flaticon.com/free-icon/web-search-engine_3003511)
is from [Freepik - Flaticon](https://www.flaticon.com/free-icons/web-page).

The Google domains listed in `manifest.json` are from
<https://www.google.com/supported_domains>.
